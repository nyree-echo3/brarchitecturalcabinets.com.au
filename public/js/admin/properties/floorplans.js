$('#added_floorplan').sortable({
    items: '.sortable',

    stop: function () {
        counter = 0;
        $('input.flrPosition').each(function () {
            $(this).val(counter);
            counter++;
        });
    }
});

for (i = 0; i < $('#floorplanCount').val(); i++) {
    $('#flrStatus' + i).bootstrapToggle();
}

function openFloorplanPopup() {
    floorplanCounter = $('#floorplanCount').val();

    CKFinder.popup({
        chooseFiles: true,
        width: 800,
        height: 600,

        onInit: function (finder) {

            finder.on('files:choose', function (evt) {
                var files = evt.data.files;

                files.forEach(function (file, i) {
                    folder = file.get('folder');

                    strHtml = '<div id="flrBox' + floorplanCounter + '" class="floorplan-box sortable">';

                    strHtml += '<input id="flrPosition' + floorplanCounter + '" name="flrPosition' + floorplanCounter + '" type="hidden" class="flrPosition" value="' + floorplanCounter + '">';
                    strHtml += '<input id="flrDelete' + floorplanCounter + '" name="flrDelete' + floorplanCounter + '" type="hidden" value="">';
                    strHtml += '<input id="flrType' + floorplanCounter + '" name="flrType' + floorplanCounter + '" type="hidden" value="new">';
                    strHtml += '<div class="floorplan-box-toolbox">';

                    strHtml += '<a class="tool" title="Delete" href="#" onclick="floorplan_delete(' + floorplanCounter + '); return false;">';
                    strHtml += '<i class="fa fa-trash-alt"></i>';
                    strHtml += '</a>';

                    strHtml += '<div class="pull-right" style="margin-right: 10px">';
                    strHtml += '<div style="width: 33px; height: 22px;">';
                    strHtml += '<input id="flrStatus' + floorplanCounter + '" name="flrStatus' + floorplanCounter + '" data-id="' + floorplanCounter + '" type="checkbox" class="floorplan_status" data-size="mini" checked>';
                    strHtml += '</div>';
                    strHtml += '</div>';

                    strHtml += '</div>';

                    strHtml += '<div class="floorplan-box-img">';
                    strHtml += '<image class="floorplan-item" src="' + folder.getUrl() + file.get('name') + '">';
                    strHtml += '<input id="flrLocation' + floorplanCounter + '" name="flrLocation' + floorplanCounter + '" type="hidden" value="' + folder.getUrl() + file.get('name') + '">';

                    strHtml += '</div>';

                    strHtml += '</div>';

                    $('#added_floorplan').append(strHtml);
                    $('#flrStatus' + floorplanCounter).bootstrapToggle();

                    floorplanCounter++;

                    $('#floorplanCount').val(floorplanCounter);
                    $('#remove-floorplan').removeClass('invisible');
                });
            });

        }

    });
}

function floorplan_delete(id) {
    $('#flrBox' + id).remove();
    $('#floorplanCount').val($('#floorplanCount').val() - 1);

    if ($('#floorplanCount').val() == 0) {
        $('#remove-floorplan').addClass('invisible');
    }
}

function floorplan_edit_delete(id) {
    $('#flrBox' + id).addClass('invisible');
    $('#flrDelete' + id).val("1");
}