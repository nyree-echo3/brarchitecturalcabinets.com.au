@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/font-awesome-old/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fas fa-donate"></i> {{ $display_name }}</a></li>             
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">

                        <div class="box-body no-padding">                            
                            <form id="message-delete-form" method="post" class="form-inline" action="{{ url('dreamcms/contact/delete') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="table-responsive mailbox-messages">
                                    @if(count($donations))
                                        <table class="table table-hover table-striped">
                                            <tbody>
                                            
                                            <tr>
												<th>Name</th>
												<th>Donation</th>								
												<th>Date</th>				
												<th class="hd-text-right">Actions</th>
											</tr>
                                           
                                            @foreach($donations as $donation)
                                                @php
                                                $properties = json_decode($donation->data);
                                                $name = '';                                               

                                                foreach($properties as $property){
                                                    if($property->field=='name'){
                                                        $name = $property->value;
                                                    }                                                   
                                                }
                                                @endphp
                                                
                                                    <tr>                                                                                                           
                                                        <td class="mailbox-name">
                                                            @can('read-donation')
                                                                <a href="{{ url('dreamcms/donations/'.$donation->id.'/read') }}">{{ $name }}</a>
                                                            @else
                                                                {{ $name }}
                                                            @endcan
                                                        </td>
                                                        <td class="">${{ $donation->amount }}</td>
                                                        <td class="mailbox-date">{{ $donation->created_at->diffForHumans() }}</td>
                                                        <td class="hd-text-right"><a href="{{ url('dreamcms/donations/'. $donation->id .'/read') }}" class="tool" data-toggle="tooltip" title="View"><i class="far fa-eye"></i></a></td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="col-xs-12">No record</div>
                                    @endif
                                </div>
                            </form>

                        </div>

                        <div class="box-footer no-padding">

                            <div class="col-xs-6 mailbox-controls">

                                <div class="mailbox-controls-container">

                                    <div class="tool-buttons">                                        
                                        <button type="button" class="btn btn-default btn-sm refresh-messages"><i class="fa fa-refresh"></i></button>
                                    </div>

                                    <div class="paginate-dropdown">
                                        <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <select id="pagination_count" name="pagination_count">
                                                <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                                <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                                <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                            </select>
                                            <span class="total-row"> Total {{ $donations->total() }} donations</span>
                                        </form>
                                    </div>

                                </div>

                            </div>

                            <div class="col-xs-6" style="text-align: right;">
                                {{ $donations->links() }}
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/fastclick/fastclick.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/js/admin/inbox.js') }}"></script>
@endsection