@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-envelope"></i> {{ $display_name }}</a></li>
                <li class="active">Details</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/contact/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">

                                    <div class="col-sm-12">
                                        <textarea id="details" name="details" rows="20" cols="80"
                                                  style="height: 750px;">{{ $details->value }}</textarea>
                                        @if ($errors->has('details'))
                                            <small class="help-block">{{ $errors->first('details') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('details');
        });
    </script>
@endsection