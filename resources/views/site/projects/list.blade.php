<?php
   // Set Meta Tags
   $meta_title_inner = "Projects";
   $meta_keywords_inner = "Projects";
   $meta_description_inner = "Projects";
?>

@extends('site/layouts/app')

@section('styles')
	<link rel="stylesheet" href="{{ asset('/css/site/projects.css') }}">
@endsection

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">
        <div class="container project-main-container">

            <div class="row">
                <div class="col-12 blog-main">

                    <div class="blog-post">

						<section class="project-block cards-project">
						   <div class="container project-block-container">
							  <div class="row">
								  <div class="col-sm-12">
									 <h1>Our Projects</h1>
								  </div>

						@if(isset($items))
							  @php
								  $item_counter = 0;
								  $col_position = 0;
								  $items_total = sizeof($items);
								  $items_last_row = $items_total % 3;
							  @endphp

							  @foreach($items as $item)
								  @php
									  $item_counter++;
									  $col_position++;

									  if ($col_position == 5) {
										 $col_position = 1;
									  }
								  @endphp

								  <div class="col-12 col-sm-4 col-md-3 project-list-box">
									   <div class="links-img-border-box project-list-item">
										   <a href="{{ url('') }}/projects/{{ $item->category->slug }}/{{$item->slug}}">
											   <div>
													@if (count($item->images) > 0)
													<div class="div-img">
													   <img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}" />
													</div>
													@endif

													<div class="project-list-h1">{{$item->title}}</div>
													@if ($item->contract_value != "") <div class="project-list-h2">Contract Value: ${{$item->contract_value}}</div> @endif
													@if ($item->construction_year != "") <div class="project-list-h3">Year of Construction: {{$item->construction_year}}</div> @endif
													@if ($item->builder != "") <div class="project-list-h3">Builder: {{$item->builder}}</div> @endif
													@if ($item->architect != "") <div class="project-list-h3">Architect: {{$item->architect}}</div> @endif
													@if ($item->client != "") <div class="project-list-h3">Client: {{$item->client}}</div> @endif
											  </div>
											  <!--<div class="projects-more">Read More ></div>-->
										  </a>
									   </div>
									</div>

									@if ($item_counter == $items_total && $col_position < 3)
										<div class="col-sm-4 project-list-box  project-list-box-none">
										   <div class="links-img-left-border">
											   <div class="links-img-border-box project-list-item"></div>
										   </div>
										</div>
									@endif

							  @endforeach

						   @else
							 <p>Currently there is no projects to display.</p>
						   @endif
						  </div><!-- /.row -->
						</div><!-- /.container -->
					   </section>

           <!-- Pagination -->
           <div id="pagination">{{ $items->links() }}</div>

         </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection
