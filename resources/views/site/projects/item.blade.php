<?php 
   // Set Meta Tags
   $meta_title_inner = "Projects"; 
   $meta_keywords_inner = "Projects"; 
   $meta_description_inner = "Projects"; 
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        <div class="col-xl-10 col-lg-10 col-md-12 blog-main">
                   
          <div class="blog-post">           
            <h1>{{ $project_item->title }}</h1>
            
            <section class="project-block cards-project">
               <div class="container">	  
                  <div class="row">	       	            
	                
                      <div class='project-item'>
						   <div class='project-item-txt'>						      						      
						      @if ( $project_item->contract_value != "")<div class='project-value'><strong>Contract Value</strong><br>${{$project_item->contract_value}}</div>@endif 
						      @if ( $project_item->construction_year != "")<div class='project-value'><strong>Construction Year</strong><br>{{$project_item->construction_year}}</div>@endif 
						      @if ( $project_item->builder != "")<div class='project-value'><strong>Builder</strong><br>{!! $project_item->builder !!}</div>@endif 
						      
						      @if ( $project_item->architect != "")<div class='project-value'><strong>Architect</strong><br>{{$project_item->architect}}</div>@endif 
							  @if ( $project_item->location != "")<div class='project-value'><strong>Location</strong><br>{{$project_item->location}}</div>@endif 
					          @if ( $project_item->client != "")<div class='project-value'><strong>Client</strong><br>{{$project_item->client }}</div>@endif
							  							    
							  <div class='project-value'>{!! $project_item->description !!}</div>
							  
							  <div class='btn-back'>
							     <a class='btn-back' href='javascript: window.history.go(-1);'>< Back</a>	
							  </div>
						   </div>

						   @if (count($project_item->images) > 0)
							   <div class='project-item-img'>							   							   							   
							   
									<!-- Carousel of images -->
									<div id="carousel-projects" class="carousel slide carousel-fade" data-ride="carousel">

									  <!-- Indicators -->
									  <ul class="carousel-indicators">
										@php $counter = 0;
										@endphp

										@foreach($project_item->images as $image)   
										   <li data-target="#carousel-projects" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>   

										   @php 
										   $counter++;         	
										   @endphp
										@endforeach

									  </ul>

									  <!-- The slideshow -->
									  <div class="carousel-inner">
										@php $counter = 0;
										@endphp

										@foreach($project_item->images as $image)  
											@php 
											$counter++;         	
											@endphp

											<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
											  <!--<a class="lightbox" href="{{ url('') }}{{$image->location}}" data-caption="{{$project_item->title}}">-->
												 <img src="{{ url('') }}{{$image->location}}" alt="{{$project_item->name}}">
											  <!--</a>-->
											</div>	
										@endforeach	
									  </div>

									  <!-- Left and right controls -->
									  <a class="carousel-control-prev" href="#carousel-projects" data-slide="prev">
										<span class="carousel-control-prev-icon"></span>
									  </a>
									  <a class="carousel-control-next" href="#carousel-projects" data-slide="next">
										<span class="carousel-control-next-icon"></span>
									  </a>

									</div>
									<!-- End of Carousel of images -->					       
						   @endif

						
					</div>
            
             
                  </div><!-- /.row -->
               </div><!-- /.container -->			             
            </section>  
                                        
                                         
         </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-project', { animation: 'slideIn'});
        });	   	   
    </script>			
@endsection
