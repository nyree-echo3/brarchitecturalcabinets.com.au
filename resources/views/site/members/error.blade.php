<?php 
   // Set Meta Tags
   $meta_title_inner = "Register | " . $company_name; 
   $meta_keywords_inner = "Register " . $company_name; 
   $meta_description_inner = "Register " . $company_name;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-members')
                     
        <div class="col-sm-8 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Register</h1>    
               <p>Thank-you {{ $member->firstName }} for registering your details.</p>   
               @include('flash::message')          
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection            
