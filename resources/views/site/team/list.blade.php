@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">                
                            
                    <div class="blog-post row">                        
                        @if($items)
                            <div class="col-lg-12">	
                               <h1>{{ $items[0]->category->name }}</h1>
						    </div>
                           
                            @foreach($items as $item)                              
								<div class="col-lg-6 team-a">
								     		
								     <div class="team-div-img">									  							  
										 <div class="div-img">
											<img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}"> 
										 </div>
									 </div>

									 <div class="team-txt">
										<div class="team-name-band-name">{{ $item->name }}</div>
										<div class="team-name-band-title">{{ $item->job_title }}</div>
										<div class="team-name-band-role">{!! $item->role !!}</div>																															
										<div class="team-name-band-description">{!! $item->short_description !!}</div>
									 </div>	

									 <a class='btn-back' href='{{ ($item->category->slug == "old-2construct-board" ? 'javascript:void(0)' : url('') . '/team/' . $item->category->slug . '/' . $item->slug) }}'>READ MORE <i class="fas fa-chevron-right"></i></a>							  										  			
								</div>                       
                            @endforeach                       
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
