@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/layerslider.css') }}">
@endsection

<div id="slider-mobile">
		
		@foreach($images as $image) 		    							
			<div class="ls-slide" data-ls="bgcolor:#ffffff; duration:4000; kenburnsscale:1.2;">
			    <img  src="{{ url('') }}/{{ substr($image->location, 0, -4) }}-mobile{{ substr($image->location, -4) }}" alt="{{ $image->title }}" class="ls-l" alt="" style="top:50%; left:50%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; mix-blend-mode:normal; width:100%;" data-ls="showinfo:1; durationin:2000; easingin:easeOutExpo; scalexin:1.5; scaleyin:1.5; static:forever; position:fixed;">			  										
			    
			    <div style="top:0px; width:100%; color:#112649; font-size:20px; background-color: #fff; text-align:center; padding:15px 0" class="ls-l" data-ls="showinfo:1; durationin:2000; easingin:easeOutExpo; scalexin:2.5; scaleyin:2.5; transformoriginin:50% 143.8% 0;">
				   {{ $image->description }}
				</div>	
				
			</div>						
		@endforeach
				
	</div>
@section('scripts')
	<script src="{{ asset('js/site/greensock.js') }}"></script>
	<script src="{{ asset('js/site/layerslider.transitions.js') }}"></script>
	<script src="{{ asset('js/site/layerslider.kreaturamedia.jquery.js') }}"></script>
@endsection
	
@section('inline-scripts-slider-mobile')
<script type="text/javascript">   
    $(document).ready(function(){        
        $('#slider-mobile').layerSlider({
				sliderVersion: '6.5.0b1',
				type: 'fullwidth',
				allowFullscreen: false,
				maxRatio: 1,
				autoStart: true,				
				globalBGSize: 'cover',
				navStartStop: false,
				showCircleTimer: false,
				thumbnailNavigation: 'disabled',				
			});
    });
 
</script>
@endsection