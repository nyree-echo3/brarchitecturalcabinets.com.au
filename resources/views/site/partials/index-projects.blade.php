@if(isset($home_projects))
	 <div class="home-projects">
       <div class="home-projects-hdr">Featured projects</div>
       
	   <div class="container">
		  <div class="row">  
		     @php
		        $counter = 0;
		     @endphp
		            
			 @foreach($home_projects as $item)       	 
				  <div class="col-lg-4 {{ ($counter > 0 ? "col-border" : "") }}">
                       <div class="home-projects-block">
						   <a href='{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}'>				
							   <div class="home-projects-a {{ ($counter == 0 ? "home-projects-a-first" : "") }}">
								 <div class="div-img">
								    @if (count($item->images) > 0)	
									   <img src="{{ url('') }}/{{($item->images[0]->location != "" ? $item->images[0]->location : "images/site/project-small-no-image.jpg") }}" alt="{{ $item->title }}">	
									@else
									   <img src="{{ url('') }}/images/site/project-small-no-image.jpg" alt="{{$item->title}}" />	
									@endif									
								 </div>
								 <div class="home-projects-txt">
									<div class="home-projects-txt-h1">{{ $item->title }}</div>
									@if ($item->contract_value != "")<div class="home-projects-txt-h2">Contract Value: ${{$item->contract_value}} </div> @endif
									@if ($item->construction_year != "")<div class="home-projects-txt-h3">Year of Construction: {{$item->construction_year}} </div> @endif
									@if ($item->builder != "")<div class="home-projects-txt-h3">Builder: {{$item->builder}} </div> @endif
									@if ($item->architect != "")<div class="home-projects-txt-h3">Architect: {{$item->architect}} </div> @endif
									@if ($item->client != "")<div class="home-projects-txt-h3">Client: {{$item->client}} </div> @endif									
								 </div>   
							   </div>							
						   </a> 
		               </div>			           				       				   					 					   					   					    
					    			           				       				   					 					   					   					 
				  </div><!-- /.col-lg-4 -->
				  
				  @php
		             $counter++;
		          @endphp
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif