<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark btco-hover-menu navbar-custom">
    <div class="panelMax">
		
		<div class="navbar-logo">
			<a href="{{ url('') }}" title="{{ $company_name }}">
			   <img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="logo-lg">
			   <img src="{{ url('') }}/images/site/logo-small.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="logo-sm">
			</a>
		</div> 
        		
		<button class="navbar-toggler custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger " type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="hamburger-box">
		<span class="hamburger-inner"></span>
	  </span>
	</button>
	
		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto">							
				{!! $navigation !!}	
				<li class="nav-item"><a class="nav-link" href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class="fas fa-phone-alt"></i> {{ $phone_number }}</a></li>							
			</ul> 						     			                			  
		</div>
   
        <div class="contact-phone-resp">
        	<a href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class="fas fa-phone-alt"></i></a>
        </div>
    </div>  
</nav>


@section('inline-scripts-navigation')
    <script type="text/javascript">
		  var $hamburger = $(".hamburger");
		  $hamburger.on("click", function(e) {
			$hamburger.toggleClass("is-active");
			// Do something else, like open/close menu
		  });
    </script>
@endsection