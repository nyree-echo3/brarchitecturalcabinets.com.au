<footer class='footer'>
 <div class='footerContactsWrapper'>          
	 <div class='footerContacts'>
        <div class="panelNav">
	       <div class="container-fluid">
			  <div class="row">

				   <div class="col-xl-3 col-lg-6 col-md-12">	
					  <div class="footer-logo">
						  <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-bottom.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>			  		  				  		  			  
					  </div>  
				   </div>	   

				   <div class="col-xl-3 col-lg-6 col-md-12">	
					  <div class="footer-txt">
						  {{ $company_name }}<br>	
						  {!! $address !!}
					  </div>  
				   </div>

				   <div class="col-xl-3 col-lg-6 col-md-12">	
					  <div class="footer-txt">
						   @if ( $phone_number != "") <strong>T</strong> <a href="tel:{{ str_replace(" ", "", $phone_number) }}">{{ $phone_number }}</a><br> @endif 
						   @if ( $email != "") <strong>E</strong> <a href="mailto:{{ $email }}">{{ $email }}</a> @endif 				 				 	  				  		  			 
					  </div>  
				   </div>

				   <div class="col-xl-3 col-lg-6 col-md-12">	
					  <div class="footer-txt">
						  &copy; {{ date('Y') }} {{ $company_name }}<br>
						  <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 				 	  				  		  			 
					  </div>  
				   </div>     	              		                      	              		                 

			</div>
	    </div> 
    </div>
 </div>    
</footer>