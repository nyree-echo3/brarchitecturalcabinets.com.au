@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/layerslider.css') }}">
@endsection

<div id="slider">
		
		@foreach($images as $image) 			
			<div class="ls-slide" data-ls="bgcolor:#ffffff; duration:4000; kenburnsscale:1.2;">
				<img  src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}" class="ls-l" alt="" style="top:50%; left:50%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; mix-blend-mode:normal; width:100%;" data-ls="showinfo:1; durationin:2000; easingin:easeOutExpo; scalexin:1.5; scaleyin:1.5; static:forever; position:fixed;">
																					
				<div style="top:160px; left:50%;font-weight:600; color:#112649; font-size:72px; background-color: rgba(255,255,255,0.70); text-align:center; line-height: 72px!important; padding:40px 30px 30px 30px;" class="ls-l" data-ls="showinfo:1; durationin:2000; easingin:easeOutExpo; scalexin:2.5; scaleyin:2.5; transformoriginin:50% 143.8% 0;">
				   {{ $image->title }}
				   <div class="slider-caption-desc">{{ $image->description }}</div>
				</div>								
				
				<a style="" class="ls-l" href="#next" target="_self" data-ls="showinfo:1; durationin:2000; delayin:200; easingin:easeOutExpo; scalexin:2.5; scaleyin:2.5; transformoriginin:50% -126.1% 0; hover:true; hoveropacity:1; hoverbgcolor:#ffff24;">					
				</a>
			</div>
		@endforeach
				
	</div>
@section('scripts')
	<script src="{{ asset('js/site/greensock.js') }}"></script>
	<script src="{{ asset('js/site/layerslider.transitions.js') }}"></script>
	<script src="{{ asset('js/site/layerslider.kreaturamedia.jquery.js') }}"></script>
@endsection
	
@section('inline-scripts-slider')
<script type="text/javascript">   
    $(document).ready(function(){        
        $('#slider').layerSlider({
				/*sliderVersion: '6.5.0b1',
				type: 'fullwidth',
				allowFullscreen: false,
				maxRatio: 1,
				autoStart: true,				
				globalBGSize: 'cover',
				navStartStop: false,
				showCircleTimer: false,
				thumbnailNavigation: 'disabled',	*/
			
			sliderVersion: '6.5.0b1',
				type: 'fullwidth',
				allowFullscreen: false,
				maxRatio: 1,
				autoStart: false,
				skin: 'v6',
				globalBGSize: 'cover',
				navStartStop: false,
				showCircleTimer: false,
				thumbnailNavigation: 'disabled',
				skinsPath: '../../layerslider/skins/'
			});
    });
 
</script>
@endsection