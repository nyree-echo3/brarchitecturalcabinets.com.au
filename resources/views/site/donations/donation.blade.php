<?php 
   // Set Meta Tags
   $meta_title_inner = "Make a Donation" . $company_name; 
   $meta_keywords_inner = "donation, " . $company_name; 
   $meta_description_inner = "Make a Donation | " . $company_name;  

?>
@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-donation')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">
             <h1>Make a Donation</h1>
             
             <form id="donation-form" class="frmDonation" method="post" action="{{ url('donation/save-donation') }}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 
				 <div class="donationWrapper">
					<div class="container">
					   <section class="radSectionAmount">
						   <div class="row">
						       @php 							         
								   $donationSelected = 0;
							   @endphp
							   						       
							   @foreach ($categories as $category)
								   <div class="col-lg-4">
									  <input type="radio" name="radAmount" id="{{ $category->id }}" value="{{ $category->amount }}" class="rad-donation" {{ (substr($donation, 1) > 0 && substr($donation, 1) == $category->amount ? 'checked' : '') }}>
									  <label for="{{ $category->id }}" class="label-amount donation-title">
										 {{ $category->title }}	
										 <div class="donation-amt">${{ $category->amount }}</div>										    
									  </label>										     
								   </div>
								   
								   @php 
							         if (substr($donation, 1) > 0 && substr($donation, 1) == $category->amount)  {
								         $donationSelected = 1;
								     }  
								   @endphp
								   
							   @endforeach							   
						   </div>  
						</section>
												
					  </div>  
				 </div>
				 
				 <div class="donationWrapper">
				    <input type="radio" id="radAmountOther" name="radAmount" id="{{ $category->id }}" value="{{ $category->amount }}" class=""  {{ (substr($donation, 1) > 0 && $donationSelected == 0 ? 'checked' : '') }}>
					Other Amount $
					<input type="text" name="amountOther" class='txtDonationAmt' value='{{ (substr($donation, 1) > 0 && $donationSelected == 0 ? substr($donation, 1) : '') }}'>
					<div class="fv-plugins-message-container" id="errorAmountOther">
						
					</div>
				 </div>
				 		
				 <div class="donationWrapper {{ (substr($donation, 1) > 0 ? 'donationWrapperInvisible' : '') }}">
				     <a id="btnDonation" href='#' class="btn-donation">Donte Now</a>   
				 </div>
				 
				 @include('flash::message')     				 
				 <div class="donationForm {{ (substr($donation, 1) > 0 ? 'donationFormVisible' : '') }}">				    						
						<div id="donation-form-fields"></div>

						<div class="form-row">
							<div class="col-12 col-sm-10 g-recaptcha-container">
								<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
								@if ($errors->has('g-recaptcha-response'))
									<div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
								@endif
							</div>
						</div>
						
						<button type="submit" class="btn-donation">Submit Donation</button>
				
				 </div>
				 
			   </form>
			</div>                   
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection


@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
		
		$(document).ready(function () {
            $('#donation-form-fields').formRender({
                dataType: 'json',
                formData: {!! $form !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('donation-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });
			
			
			$("#btnDonation").click(function(){
			  $(".donationForm").show();
			  $("#btnDonation").hide();
			});
			
			$(".rad-donation").click(function(){				
			   $('input[name = "donation"]').val(("$" + $('input[name = "radAmount"]:checked').val()));
			   $('input[name = "amountOther"]').val('');
			   $('#errorAmountOther').html(''); 	
            });
			
			$('input[name = "amountOther"]').focusout(function(){	
			   if ( isNaN( $('input[name = "amountOther"]').val() ))  { 
				   
				  $('input[name = "amountOther"]').val('');
				  $('input[name = "donation"]').val('');
				   
				  $('#errorAmountOther').html('<div class="fv-help-block" data-field="name" data-validator="notEmpty">The field is not valid.  It must be numeric and greater than $5.00.</div>');
			   } else if ($('input[name = "amountOther"]').val() < 5) {
				  $('input[name = "amountOther"]').val(''); 
				  $('input[name = "donation"]').val(''); 
				   
				   $('#errorAmountOther').html('<div class="fv-help-block" data-field="name" data-validator="notEmpty">The field is not valid.  It must be numeric and greater than $5.00.</div>');
			   } else  {
			      $('input[name = "donation"]').val( "$" + parseFloat($('input[name = "amountOther"]').val()).toFixed(2) );
				  $('input[name = "amountOther"]').val( parseFloat($('input[name = "amountOther"]').val()).toFixed(2) ); 
				   
				  $('#errorAmountOther').html(''); 
			   }
            });
			
			$('input[name = "amountOther"]').focus(function() {
               $('#radAmountOther').prop("checked", true); 
				
				
            });
			
        });
		
    </script>
@endsection		