<!DOCTYPE html>
<html>
<head>
    <style>
        body { font: 14px/18px normal Arial,Helvetica,sans-serif; color: #333333;}
    </style>
</head>
<body>
<table width="800">
    <tr>
        <td><img src="{{ url('') }}/images/site/logo.png"></td>
    </tr>
    <tr>
        <td>
            <p>Hi there</p>
            <p>Well done on completing your personalEYES Online Eye Test! As the one of the most experienced corrective eye surgery groups in Australasia, we're pleased to be able to support you on your journey towards 20 20 vision.</p>
            <p><strong>Here are your online test results:</strong></p>
            <p>{{ $result }}</p>
            <p><strong>Want to learn more about how Lasik works?</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <a href="https://lasik.personaleyes.com.au/pages/all-about-lasik/what-is-lasik" target="_blank">
                <img src="{{ url('') }}/images/site/email-lasik-video.png">
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <table width="200">
                <tr>
                    <td style="background-color: #0071ad; text-align: center; font-size: 12px; font-weight: bold; padding: 15px 15px 15px 15px;">
                        <a href="https://lasik.personaleyes.com.au/pages/all-about-lasik/what-is-lasik">Watch this short video here</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>What to calculate how much you could be saving with Lasik?</strong></p>
            <p>Use our simple online calculator to see the savings you could be making with our affordable laser eye surgery.</p>
        </td>
    </tr>
    <tr>
        <td>
            <table width="250">
                <tr>
                    <td style="background-color: #0071ad; text-align: center; font-size: 12px; font-weight: bold; padding: 15px 15px 15px 15px;">
                        <a href="https://lasik.personaleyes.com.au/pages/all-about-lasik/what-is-lasik">Click here for online calculator</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><p>Feel free to hit reply, hop onto our website chat or give us a call on 1300 683 937 if you have any questions!</p></td>
    </tr>
    <tr>
        <td><p>Warm regards, <br />The team at personalEYES<p></td>
    </tr>
    <tr>
        <td><a href="https://lasik.personaleyes.com.au" target="_blank">lasik.personaleyes.com.au</a></td>
    </tr>
    <tr>
        <td>
            <p>
            Toll Free 1300 68 3937 [1300 Nu Eyes]<br />
            P (02) 8833 7111<br />
            F (02) 8833 7112 or 1800 789 077<br />
            E <a href="mailto:vision@personaleyes.com.au">vision@personaleyes.com.au</a><br />
            PO Box 301 Parramatta, NSW, 2150
            <p>
        </td>
    </tr>
</table>
</body>
</html>
