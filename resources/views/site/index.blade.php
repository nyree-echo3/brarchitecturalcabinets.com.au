@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')
@include('site/partials/carousel-mobile')

@include('site/partials/index-projects')

@endsection
