<?php
// Set Meta Tags
$meta_title_inner = $page->meta_title;
$meta_keywords_inner = $page->meta_keywords;
$meta_description_inner = $page->meta_description;
?>

@extends('site/layouts/app')
@section('content')

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        @if (isset($page))
                            <h1 class="blog-post-title">{{$page->title}}</h1>
                            {!! $page->body !!}
                        @endif
                    </div><!-- /.blog-post -->
                    @if($page->slug=='savings-calculator')
                        @include('site/savings-calculator/calculator')
                    @endif
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->    
@endsection