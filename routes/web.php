<?php 
// Home Page
Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@indexDev');  // To bypass holding page in development mode

// Content Styles
Route::get('/style-guide', 'HomeController@styleGuide');

// Make A Booking
Route::get('/make-a-booking', 'HomeController@makeABooking');

// Referrals
Route::group(['prefix' => '/refer-a-friend'], function () {
    Route::get('/', 'ReferralController@referAFriend');
	Route::post('save', 'ReferralController@save');
	Route::get('complete', 'ReferralController@complete');
});

// Contact Form
Route::group(['prefix' => '/contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::get('success', 'ContactController@success');
	Route::post('save-message-golive', 'ContactController@saveMessageGolive');
});

// Pages Module
Route::group(['prefix' => '/pages'], function () {
	Route::get('all-about-lasik', 'PagesController@lasikGuide'); 
    Route::get('{category}', 'PagesController@index');    
	Route::get('{category}/{page}', 'PagesController@index');    
});

// News Module
Route::group(['prefix' => '/news'], function () {
	Route::get('', 'NewsController@list'); 	
	
	Route::get('archive', 'NewsController@archive'); 
	Route::get('archive/{age}', 'NewsController@archive'); 
	
    Route::get('{category}', 'NewsController@list');    
	Route::get('{category}/{page}', 'NewsController@item'); 
	
});

// Faqs Module
Route::group(['prefix' => '/faqs'], function () {
	Route::get('', 'FaqsController@index'); 
    Route::get('{category}', 'FaqsController@index');    	
});

// Documents Module
Route::group(['prefix' => '/documents'], function () {
	Route::get('', 'DocumentsController@index'); 
    Route::get('{category}', 'DocumentsController@index');    	
});

// Gallery Module
Route::group(['prefix' => '/gallery'], function () {
	Route::get('', 'GalleryController@index'); 
    Route::get('{category}', 'GalleryController@detail');    	
});

// Projects Module
Route::group(['prefix' => '/projects'], function () {
	Route::get('', 'ProjectsController@list'); 			
	
    Route::get('{category}', 'ProjectsController@list');    
	Route::get('{category}/{page}', 'ProjectsController@item');
});

// Properties Module
Route::group(['prefix' => '/properties'], function () {
    Route::get('{category}', 'PropertiesController@list');
    Route::get('{category}/{page}', 'PropertiesController@item');
});


// Team Module
Route::group(['prefix' => '/team'], function () {
    Route::get('', 'TeamController@index');
    Route::get('{category}', 'TeamController@index');
    Route::get('{category}/{member}', 'TeamController@member');
});

// Donation Module
Route::group(['prefix' => '/donation'], function () {
    Route::get('/', 'DonationsController@index');
    Route::post('save-donation', 'DonationsController@saveDonation');
    Route::get('success/{donation}', 'DonationsController@success');
	Route::get('error/{donation}', 'DonationsController@success');
});

// Locations Module
Route::group(['prefix' => '/locations'], function () {
	Route::get('', 'LocationsController@index'); 
    Route::get('/{location}', 'LocationsController@item');    	
});

// ***********
// Members Module
// ***********
// Register
Route::group(['prefix' => '/register'], function () {    
	Route::get('/', 'Auth\RegisterController@register');	
    Route::post('store', 'Auth\RegisterController@store');
    Route::get('success/{member}', 'Auth\RegisterController@success');			
}); 

// Login
Route::group(['prefix' => '/login'], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm');
	Route::post('/', 'Auth\LoginController@login');
	
	
   // Route::post('process', 'MembersController@process');
	
	//Route::get('forgot', 'MembersController@forgot');
}); 

// Logout
Route::group(['prefix' => '/logout'], function () {
    Route::get('/', 'Auth\LoginController@logout');	
}); 

// Password Reset
Route::group(['prefix' => '/password'], function () {
    Route::get('forgot', 'Auth\ForgotPasswordController@showLinkRequestForm');	
	Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail');	
	
	Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm');	
	Route::post('reset/', 'Auth\ResetPasswordController@reset');	
}); 

// Members Only Section
Route::group(['prefix' => '/members-portal'], function () {
    Route::get('/', 'MembersController@index');
	
	Route::get('change-details', 'MembersController@changeDetails');
	Route::post('save-details', 'MembersController@saveDetails');
	
	Route::get('change-password', 'MembersController@changePassword');
	Route::post('save-password', 'MembersController@savePassword');
}); 


// ***********
// Shop Module
// ***********
Route::group(['prefix' => '/products', 'as' => 'products.'], function() {
    Route::get('', 'ProductsController@list')->name('list'); 	
	
	Route::get('{category}', 'ProductsController@list');    
	Route::get('{category}/{page}', 'ProductsController@item'); 	
});

Route::group(['prefix' => 'cart', 'as' => 'cart.'], function() {
    Route::get('show', 'CartController@show')->name('show');
    Route::post('add/{product}', 'CartController@add')->name('add');
    Route::post('remove/{cart_item}', 'CartController@remove')->name('remove');
});

Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], function() {
    Route::get('show', 'CheckoutController@show')->name('show');	
    Route::post('submit', 'CheckoutController@submit')->name('submit');
		
	Route::get('complete/{order}', 'CheckoutController@complete')->name('complete');
	Route::get('cancel/{order}', 'CheckoutController@cancel')->name('cancel');
});
// ***********

// Testimonials Module
Route::group(['prefix' => '/testimonials'], function () {
    Route::get('', 'TestimonialsController@index');
});

// Online Eye Test
Route::group(['prefix' => '/online-eye-test'], function () {
    Route::get('step1', 'OnlineEyeTestController@step1');
    Route::any('step2', 'OnlineEyeTestController@step2');
    Route::any('step3', 'OnlineEyeTestController@step3');
    Route::any('step4', 'OnlineEyeTestController@step4');
    Route::any('step5', 'OnlineEyeTestController@step5');
    Route::any('step6', 'OnlineEyeTestController@step6');
    Route::any('end-test', 'OnlineEyeTestController@endTest');
    Route::get('email', 'OnlineEyeTestController@email');
    Route::any('send-result', 'OnlineEyeTestController@sendResult');
    Route::get('send-result-test', 'OnlineEyeTestController@sendResultTest');
    Route::get('feedback', 'OnlineEyeTestController@feedback');
});
