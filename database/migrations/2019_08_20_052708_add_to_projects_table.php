<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->text('contract_value')->nullable()->after('description');	
			$table->text('construction_year')->nullable()->after('contract_value');	
			$table->text('builder')->nullable()->after('construction_year');	
			$table->text('architect')->nullable()->after('builder');	
			$table->text('client')->nullable()->after('architect');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            //
        });
    }
}
