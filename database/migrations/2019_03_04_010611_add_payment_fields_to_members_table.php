<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentFieldsToMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->string('payment_type')->nullable()->after('is_deleted');
			$table->enum('payment_status', ['pending','completed','cancelled'])->default('pending')->after('payment_type');
			$table->string('payment_transaction_number')->nullable()->after('payment_status');
			$table->string('payment_transaction_result')->nullable()->after('payment_transaction_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            //
        });
    }
}
