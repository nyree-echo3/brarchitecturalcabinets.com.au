<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TeamMember;
use App\TeamCategory;
use App\Module;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        $module = Module::where('slug', '=', "team")->first();
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        if($request->category==''){
            $selected_category = TeamCategory::where('status','=','active')->whereHas('members')->orderBy('position', 'desc')->first();
        }else{
            $selected_category = TeamCategory::where('slug','=',$request->category)->first();
        }

        $items = null;
        $meta_title_inner = "Team";
        $meta_keywords_inner = "Team";
        $meta_description_inner = "Team";

        if($selected_category){
            $items = TeamMember::where('category_id','=', $selected_category->id)->orderBy('position', 'desc')->paginate();

            $meta_title_inner = $selected_category->name. " - Team";
            $meta_description_inner = $selected_category->name . " - Team";
        }

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation();

        $side_nav_mode = 'manual';
        if($side_navV2==null){
            $side_navV2 = $categories;
            $side_nav_mode = 'auto';
        }

        return view('site/team/list', array(
			'module' => $module,
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
            'categories' => $categories,
            'category' => $selected_category,
            'items' => $items,
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner,
			'category_name' => "Why Us?",
        ));

    }

    public function member($category, $team_member)
    {
		$module = Module::where('slug', '=', "team")->first();
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();
        $category = TeamCategory::where('slug','=',$category)->first();
        $member = TeamMember::where('slug','=',$team_member)->first();

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation($member->category->url);

        $side_nav_mode = 'manual';
        if($side_navV2==null){
            $side_navV2 = $categories;
            $side_nav_mode = 'auto';
        }

        return view('site/team/item', array(
			'module' => $module,
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
            'categories' => $categories,
            'category' => $category,
            'team_member' => $member,
            'meta_title_inner' => $member->name.'- Team',
            'meta_keywords_inner' => $member->name,
            'meta_description_inner' => $member->name,
			'category_name' => "Why Us?",
        ));
    }
}
