<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Project;
use App\ProjectCategory;

class ProjectsController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation();

        $side_nav_mode = 'manual';
        if($side_navV2==null){
            $side_navV2 = $this->getCategories();
            $side_nav_mode = 'auto';
        }

		if (sizeof($this->getCategories()) > 0)  {
			if ($category_slug == "")  {
			   // Get Latest Projects
			   $category_name = "Latest Projects";	

			   $items = $this->getProjects();
			} elseif ($category_slug != "" && $item_slug == "") {
			  // Get Category Projects	
			  $category = $this->getCategory($category_slug);
			  $category_name = $category->name;	

			  $items = $this->getProjects($category->id);			  
			} 		
		}

		return view('site/projects/list', array(
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
			//'category_name' => (sizeof($this->getCategories()) > 0 ? $category_name : null),
			'category_name' => "Projects",
			'items' => (sizeof($this->getCategories()) > 0 ? $items : null),
        ));

    }
	
    public function item($category_slug, $item_slug){

		$project_item = $this->getProjectItem($item_slug);

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation($project_item->category->url);

        $side_nav_mode = 'manual';
        if($side_navV2==null){
            $side_navV2 = $this->getCategories();
            $side_nav_mode = 'auto';
        }

		return view('site/projects/item', array(
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
			'project_item' => $project_item,	
			'category_name' => "Projects"
        ));
    }		
	public function getCategories(){
		$categories = ProjectCategory::where('status', '=', 'active')->get();
        foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getProjects($category_id = "", $limit = 12){
		if ($category_id == "")  {
			$projects = Project::where('status', '=', 'active')						
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $projects = Project::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($projects);
	}	
	  
	public function getProjectItem($item_slug){		
		$project = Project::where(['status' => 'active', 'slug' => $item_slug])
			        ->where('status', '=', 'active')
			        ->first();						
		return($project);
	}
	
	public function getCategory($category_slug){
		$categories = ProjectCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
}
