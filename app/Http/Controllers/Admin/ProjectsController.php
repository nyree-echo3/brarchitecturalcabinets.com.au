<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\Project;
use App\ProjectCategory;
use App\ProjectImage;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\SpecialUrl;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'projects')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {				
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $projects = Project::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $projects = Project::with('category')->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('projects-filter');
        $categories = ProjectCategory::orderBy('created_at', 'desc')->get();
        return view('admin/projects/projects', array(
            'projects' => $projects,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = ProjectCategory::orderBy('created_at', 'desc')->get();
        $images = array();

        if(old('imageCount')){

            for($i=0; $i<old('imageCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('imgId'.$i);
                $properties->position = old('imgPosition'.$i);
                $properties->delete = old('imgDelete'.$i);
                $properties->status = old('imgStatus'.$i);
                $properties->location = old('imgLocation'.$i);
                $properties->type = old('imgType'.$i);

                $images[$i] = $properties;
            }

            $images = collect($images)->sortBy('position');
        }

        return view('admin/projects/add', array(
            'categories' => $categories,
            'images' => $images
        ));
    }

    public function edit($project_id)
    {
        $project = Project::where('id', '=', $project_id)->with("images")->first();

        $project->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $project->id)->where('module', '=', 'projects')->where('type', '=', 'item')->first();
        if ($special_url) {
            $project->special_url = $special_url->url;
        }

        $categories = ProjectCategory::orderBy('created_at', 'desc')->get();

        if(old('imageCount')){

            for($i=0; $i<old('imageCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('imgId'.$i);
                $properties->position = old('imgPosition'.$i);
                $properties->delete = old('imgDelete'.$i);
                $properties->status = old('imgStatus'.$i);
                $properties->location = old('imgLocation'.$i);
                $properties->type = old('imgType'.$i);

                $images[$i] = $properties;
            }

            $project->images = collect($images)->sortBy('position');
        }

        return view('admin/projects/edit', array(
            'project' => $project,
            'categories' => $categories
        ));
    }

    public function store(Request $request)
    {
        $rules = array(            
            'title' => 'required',
            'slug' => 'required|unique_store:projects',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            //'short_description' => 'required',	
			//'description' => 'required',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
        );

        $messages = [
            'title.required' => 'Please select title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_store' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            //'short_description.required' => 'Please enter short desciption',
			//'description.required' => 'Please enter desciption',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/projects/add')->withErrors($validator)->withInput();
        }

        $project = new Project();
        $project->category_id = $request->category_id;
        $project->title = $request->title;
		$project->slug = $request->slug;        
        $project->meta_title = $request->meta_title;
        $project->meta_keywords = $request->meta_keywords;
        $project->meta_description = $request->meta_description;
        $project->short_description = $request->short_description;
		$project->description = $request->description;
		$project->contract_value = $request->contract_value;
		$project->construction_year = $request->construction_year;
		$project->builder = $request->builder;
		$project->architect = $request->architect;
		$project->client = $request->client;
        if($request->live=='on'){
           $project->status = 'active'; 
        }

        $project->save();

        ///////////special URL//
        if ($request->special_url != "") {

            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $project->id;
            $new_special_url->module = 'projects';
            $new_special_url->type = 'item';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();
        }
        ////////////////////////

        $this->storeImages($request, $project->id);
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/projects/' . $project->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/projects')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}
    }

	public function storeImages(Request $request, $project_id)
    {

		$deleteCount = 0;
		
		for($i = 0; $i < $request->imageCount; $i++)  {
			if (isset($request["imgDelete" . $i]) && $request["imgDelete" . $i] == 1)  {

                if($request["imgType" . $i]!="new"){
                    // Delete Image
                    $project_image = ProjectImage::where('id','=',$request["imgId" . $i])->first();
                    $project_image->delete();
                    $deleteCount++;
                }

			} else  {
				// Save Image (New or Update)
                if($request["imgType" . $i]=="new"){
				   $image = new ProjectImage();
				} else  {
				   $image = ProjectImage::where('id','=',$request["imgId" . $i])->first();	
				}

				$image->project_id = $project_id;
				$image->location = $request["imgLocation" . $i]; 
				$image->position = $request["imgPosition" . $i] - $deleteCount; 
				if($request["imgStatus" . $i]=='on'){
				   $image->status = 'active'; 
				} else {
				   $image->status = 'passive'; 	
				}

				$image->save();
			}
		}
    }
	
    public function update(Request $request)
    {
        $rules = array(
            'title' => 'required',
			'slug' => 'required|unique_update:projects,' . $request->id,
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            //'short_description' => 'required',            
            //'description' => 'required',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:projects,item,' . $request->id,
        );

        $messages = [
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_update' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            //'short_description.required' => 'Please enter short desciption',           
            //'description.required' => 'Please enter description',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'

        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/projects/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $project = Project::where('id','=',$request->id)->first();
        $project->category_id = $request->category_id;
        $project->title = $request->title;
        $project->slug = $request->slug;
        $project->meta_title = $request->meta_title;
        $project->meta_keywords = $request->meta_keywords;
        $project->meta_description = $request->meta_description;
        $project->short_description = $request->short_description;        
        $project->description = $request->description;
		$project->contract_value = $request->contract_value;
		$project->construction_year = $request->construction_year;
		$project->builder = $request->builder;
		$project->architect = $request->architect;
		$project->client = $request->client;
		if($request->live=='on'){
           $project->status = 'active'; 
        } else {
			$project->status = 'passive'; 
		}
		
        $project->save();

        //////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $project->id)->where('module', '=', 'projects')->where('type', '=', 'item')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();
            } else {
                $special_url->delete();
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $project->id;
                $new_special_url->module = 'projects';
                $new_special_url->type = 'item';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();
            }
        }
        ////////////////////////

        $this->storeImages($request, $project->id);
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/projects/' . $project->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/projects')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
		
        
    }

    public function delete($project_id)
    {
        $project = Project::where('id','=',$project_id)->first();
        $project->is_deleted = true;
        $project->save();

        SpecialUrl::where('item_id', '=', $project->id)->where('module', '=', 'projects')->where('type', '=', 'item')->delete();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeProjectStatus(Request $request, $project_id)
    {
        $project = Project::where('id', '=', $project_id)->first();
        if ($request->status == "true") {
            $project->status = 'active';
        } else if ($request->status == "false") {
            $project->status = 'passive';
        }
        $project->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $projects = Project::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/projects/sort', array(
            'projects' => $projects
        ));
    }

    public function categories(Request $request)
    {		       		
        $categories = ProjectCategory::orderBy('position', 'desc')->get();
        return view('admin/projects/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/projects/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'slug' => 'required|unique_store:project_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store'
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'slug.unique_store' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/projects/add-category')->withErrors($validator)->withInput();
        }

        $category = new ProjectCategory();
        $category->name = $request->name;		
		$category->slug = $request->slug;
		$category->description = $request->description;
		if($request->live=='on'){
           $category->status = 'active'; 
        }
        $category->save();

        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'projects';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        (new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'projects', 'category', $category->slug);

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/projects/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/projects/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}		        

    }

    public function editCategory($category_id)
    {
        $category = ProjectCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'projects')->where('type', '=', 'category')->first();
        if ($special_url) {
            $category->special_url = $special_url->url;
        }

        return view('admin/projects/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',	
			'slug' => 'required|unique_update:project_categories,'.$request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:projects,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'slug.unique_update' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/projects/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = ProjectCategory::findOrFail($request->id);

        (new NavigationHelper())->navigationItems('update', 'category', 'projects', $category->slug, null, $request->name, $request->slug);

        $category->name = $request->name;	
		$category->slug = $request->slug;
		$category->description = $request->description;
		if($request->live=='on'){
           $category->status = 'active';
            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'projects', 'category', $category->slug);
		} else {
			$category->status = 'passive';
            (new NavigationHelper())->navigationItems('delete-item', 'category', 'projects', $category->slug);
        }
        $category->save();

        (new NavigationHelper())->navigationItems('change-href', 'category', 'projects', $category->slug, $category->url);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'projects')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'projects', $category->slug, $request->special_url);
            } else {
                $special_url->delete();
                (new NavigationHelper())->navigationItems('change-href', 'category', 'projects', $category->slug, 'projects/'.$category->slug);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'projects';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'projects', $category->slug, $request->special_url);
            }
        }
        ////////////////////////

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/projects/' . $category->id . '/edit-type')->with('message', Array('text' => 'Category has been update', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/projects/categories')->with('message', Array('text' => 'Category has been update', 'status' => 'success'));
		}	
		        
    }

    public function deleteCategory($category_id)
    {
        $category = ProjectCategory::where('id','=',$category_id)->first();

        if(count($category->projects)){
            return \Redirect::to('dreamcms/projects/categories')->with('message', Array('text' => 'Category has projects. Please delete projects first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'projects')->where('type', '=', 'category')->delete();

        (new NavigationHelper())->navigationItems('delete-item', 'category', 'projects', $category->slug);

        return \Redirect::to('dreamcms/projects/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('projects-filter');
        return redirect()->to('dreamcms/projects');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->type && $request->type != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('projects-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('projects-filter')) {
            $filter_control = true;
        }

        if(!$filter_control) {
            $request->session()->put('projects-filter', [
                'category' => null,
                'search' => null
            ]);
        }

        $session = session()->get('projects-filter');

        if($session['category']==null && $session['search']==null){
            $filter_control = false;
        }

        return $filter_control;
    }
	
	public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = ProjectCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';

            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'projects', 'category', $category->slug);

        } else if ($request->status == "false") {
            $category->status = 'passive';
            (new NavigationHelper())->navigationItems('delete-item', 'category', 'projects', $category->slug);
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = ProjectCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/projects/sort-category', array(
            'categories' => $categories
        ));
    }

}