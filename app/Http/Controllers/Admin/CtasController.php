<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module;
use App\Cta;
use App\Setting;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CtasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'ctas')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {       
        $paginate_count = session()->get('pagination-count');

        $ctas = Cta::orderBy('id', 'desc')->paginate($paginate_count);
       
        return view('admin/ctas/ctas', array(
            'ctas' => $ctas,            
        ));
    }

    public function add()
    {        
        return view('admin/ctas/add', array(           
        ));
    }

    public function edit($cta_id)
    {
        $cta = Cta::where('id', '=', $cta_id)->first();    
		
		// Form Builder
		$form = Setting::where('key', '=', 'contact-form-fields')->first();		
		
        return view('admin/contact/form-builder', array(
            'form' => $form->value
        ));
		
        return view('admin/ctas/edit', array(
            'cta' => $cta,           
        ));
    }
	
    public function store(Request $request)
    {
        $rules = array(            
            'title' => 'required',
            'button' => 'required',
			'url' => 'required'
        );

        $messages = [           
            'title.required' => 'Please enter title',
            'button.required' => 'Please enter button text',
			'url.required' => 'Please enter URL'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/ctas/add')->withErrors($validator)->withInput();
        }

        $cta = new Cta();       
        $cta->title = $request->title;
		$cta->button = $request->button;
		$cta->url = $request->url;
        $cta->script = $request->script;
        $cta->image = $request->image;
		
        if($request->live=='on'){
           $cta->status = 'active'; 
        }

        $cta->save();
   
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/ctas/' . $cta->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/ctas/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(           
            'title' => 'required',
            'button' => 'required',
			'url' => 'required'
        );

        $messages = [        
            'title.required' => 'Please enter title',
            'button.required' => 'Please enter button text',
			'url.required' => 'Please enter URL'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/ctas/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $cta = Cta::where('id','=',$request->id)->first();       
        $cta->title = $request->title;
		$cta->button = $request->button;
		$cta->url = $request->url;
        $cta->script = $request->script;
		$cta->image = $request->image;
		
		if($request->live=='on'){
           $cta->status = 'active'; 
		} else {
			$cta->status = 'passive';
        }
        $cta->save();
    
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/ctas/' . $faq->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/ctas/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
    }

    public function delete($cta_id)
    {
        $cta = Cta::where('id','=',$cta_id)->first();
        $cta->is_deleted = true;
        $cta->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeCtaStatus(Request $request, $cta_id)
    {
        $cta = Cta::where('id', '=', $cta_id)->first();
        if ($request->status == "true") {
            $cta->status = 'active';
        } else if ($request->status == "false") {
            $cta->status = 'passive';
        }
        $cta->save();

        return Response::json(['status' => 'success']);
    }    
}